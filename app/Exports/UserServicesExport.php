<?php

namespace App\Exports;
use Auth;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Service;
class UserServicesExport implements FromQuery, WithHeadings
{
    public function query()
    {
        $user = Auth::user();
        $user_id = $user->id;
        return Service::where('user_id', $user_id);
    }
    public function headings(): array
    {
        return ["id", "user_id", "name","status", "created_at", "updated_at"];
    }
}
