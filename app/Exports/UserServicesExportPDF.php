<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Service;
use Auth;

class UserServicesExportPDF implements FromQuery, WithHeadings
{
    use Exportable;

    public function query()
    {
        $user = Auth::user();
        $user_id = $user->id;
        return Service::query()->where('user_id', $user_id);
    }
    public function headings(): array
    {
        return ["id", "user_id", "name","status", "created_at", "updated_at"];
    }
}
