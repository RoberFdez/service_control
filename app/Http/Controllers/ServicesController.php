<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Auth;
use App\Exports\UserServicesExport;
use Maatwebsite\Excel\Facades\Excel;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $user_id = $user->id;
        $userServices['userServices'] = Service::where('user_id', $user_id)->get();
        return view('services.show', $userServices);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $this->validate(request(),[
            'name' => 'required',
            'status' => 'required'
        ]);     

        $service = new Service();
        $service->name = $request['name'];
        $service->status = $request['status'];
        $service->user_id = $user->id;
        $service->save();
        return redirect('show-services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service['service'] = Service::where('id', $id)->get();
        return view('services.edit', $service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required',
            'status' => 'required'
        ]); 
        // print_r($request['status']);die();
        Service::where('id', $request['id'])
                ->update([
                    'name' => $request['name'],
                    'status' => $request['status']
                ]);
        return redirect('show-services');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Service::where('id',$id)->delete();
        return redirect()->route('show-services');
    }

    public function exportServicesCSV(){
        return Excel::download(new UserServicesExport, 'user_services.csv');
    }
    public function exportServicesPDF(){
        return Excel::download(new UserServicesExport, 'user_services.pdf');
    }
}
