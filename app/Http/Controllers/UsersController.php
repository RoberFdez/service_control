<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::where('rol_id', 2)->get();
        $allUsers['users'] = $users;

        $object = [
            'status' => 'OK',
            'data' => ['users' => []]
        ];

        foreach ($users as $user) {
            $services = [];
            foreach ($user->services as $service) {
                $services[] = [
                    'id' => $service->id,
                    'name' => $service->name
                ];
            }
            $object['data']['users'][] = [
                'id' => $user->id,
                'name' => $user->name,
                'services' => $services
            ];
        }
        $jsonObject = json_encode($object);
        $allUsers['jsonObject'] = $jsonObject;
        return view('users.show', $allUsers);
    }

    public function deactivateUser($id)
    {
        User::where('id', $id)
                ->update([
                    'status' => 0
                ]);
        return redirect('show-users');
    }

    public function activateUser($id)
    {
        User::where('id', $id)
                ->update([
                    'status' => 1
                ]);
        return redirect('show-users');
    }
    public function editUser(Request $request)
    {
        request()->validate([
            'userImg' => 'required|image|mimes:jpeg,png,jpg',
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
        ]);
        $user = Auth::user();
        $user_id = $user->id;

        $imageName = "user_id_" .$user_id. "_time_".time().'.'.request()->userImg->getClientOriginalExtension();
        request()->userImg->move(public_path('img/users_img'), $imageName);

        User::where('id', $user_id)
            ->update([
                'img' => $imageName,
                'name' => $request['name'],
                'email' => $request['email']                    
            ]);
            return redirect()
                   ->back()
                   ->with('success', 'Your information has update successful!');
    }
}
