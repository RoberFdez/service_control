<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
class userStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $userStatus = User::where('id', $user_id)->get();
        if($userStatus[0]->status == 1){
            return $next($request);
        }
        Auth::logout();
        return redirect()->route('login')->withErrors(["You don't have permissions to access"]);
        
    }
}
