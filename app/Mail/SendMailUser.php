<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class SendMailUser extends Mailable
{
    use Queueable, SerializesModels;
    public $userName;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userInfo)
    {
        $this->userName = $userInfo['name'];
        $this->email = $userInfo['email'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $userInfo['name'] = $this->userName;
        $userInfo['email'] = $this->email;
        return $this->view('users.email', $userInfo);
    }
}
