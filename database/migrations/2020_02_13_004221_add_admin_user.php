<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Rol;

class AddAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $adminUser = [
            'name' => 'Fdez',
            'rol_id' => 1,
            'age' => '15',
            'gender' => 'MASCULINO',
            'email' => 'fdez@gmail.com',
            'password' => bcrypt('1234abcd-'),
        ];
        DB::table('users')->insert($adminUser);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->where('email','fdez@gmail.com')->delete();
    }
}