<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecordsRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = [
            'rol' => 'admin'
        ];
        $user = [
            'rol' => 'user'
        ];
        DB::table('roles')->insert($admin);
        DB::table('roles')->insert($user);
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('roles')->where('rol','admin')->delete();
        DB::table('roles')->where('rol','user')->delete();
    }
}
