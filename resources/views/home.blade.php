@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (Auth::user()->rol_id == 2)
                        <a href="{{route('show-services')}}">CRUD - Servicess</a>                        
                    @endif
                    @if (Auth::user()->rol_id == 1)
                        <a href="{{route('show-users')}}">Users info</a>                        
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
