@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>
                <div class="panel-body">
                    <div class="card-body">
                        <?php //print_r($users); ?>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Age</th>
                                    <th>Gander</th>
                                    <th>Rol</th>
                                    <th>Status</th>
                                    <th>Active | Deactivate</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->age }}</td>
                                        <td>{{ $user->gender }}</td>
                                        <td>{{ $user->rol_id }}</td>
                                        <td>{{ $user->status == 1 ? 'Active' : 'Inactive' }}</td>
                                        <td>
                                            @if ($user->status == 1)
                                                <a class="btn btn-danger" href="{{route('deactivate-user', ['id' => $user->id])}}">Deactivate</a>                                                
                                            @else
                                            <a class="btn btn-primary" href="{{route('active-user', ['id' => $user->id])}}">Active</a>                                                
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary" data-toggle="collapse" data-target="#demo">Json object</button>
                            </div>
                        </div>
                        <br>
                        <div id="demo" class="collapse text-center">
                            <textarea class="form-control" cols="40" rows="10">
                                {{$jsonObject}}
                            </textarea>
                        </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection