<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <style>
        .avatar {
            vertical-align: middle;
            width: 50px;
            height: 50px;
            border-radius: 50%;
        }
        .avatarShow {
            vertical-align: middle;
            width: 100px;
            height: 100px;
            border-radius: 50%;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>  
                                    <li>
                                        <a href="#" onclick="showModalSettings()">Settings</a>
                                    </li>                                  
                                </ul>
                            </li>
                            <li>
                                @if (Auth::user()->img != 'default.png')
                                    <img src="{{url('img/users_img/')}}/{{Auth::user()->img}}" alt="" alt="Avatar" class="avatar">                                                                        
                                @else
                                    <img src="{{url('img/users_img/default.png')}}" alt="" alt="Avatar" class="avatar">                                    
                                @endif
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block container text-center">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                    <strong>{{ $message }}</strong>
            </div>
        @endif

        @yield('content')
    </div> 

    @if (session('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        </div>
    @endif

    @if (Auth::check())
        <!-- Start modal Settings-->
        <div class="modal fade" id="modalSettings" role="dialog">
            <div class="modal-dialog">        
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">User settings</h4>
                </div>
                <div class="modal-body">                
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">User image: </label>                        
                        </div>
                        <div class="col-md-2">
                            @if (Auth::user()->img != 'default.png')
                                <img src="{{url('img/users_img/')}}/{{Auth::user()->img}}" alt="" alt="Avatar" class="avatarShow">                                                                        
                            @else
                                <img src="{{url('img/users_img/default.png')}}" alt="" alt="Avatar" class="avatarShow">                                    
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">User name: </label>                        
                        </div>
                        <div class="col-md-2">
                            {{Auth::user()->name}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">User email: </label>                        
                        </div>
                        <div class="col-md-3">
                            {{Auth::user()->email}}
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-xs pull-right" data-toggle="collapse" data-target="#editUserInfo">
                                <span class="glyphicon glyphicon-edit"></span> Edit
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div id="editUserInfo" class="col-md-12 collapse">
                            <div class="col-md-8">
                                <hr>
                                <form action="{{route('edit-user')}}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="userImg">Select new image</label>
                                        <input type="file" class="form-control" name="userImg" placeholder="New name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">New name</label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">New email</label>
                                        <input type="email" name="email" class="form-control" required>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal">
                                                <span class="glyphicon glyphicon-remove"></span> Cancel
                                            </button>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-success btn-sm">
                                                <span class="glyphicon glyphicon-floppy-disk"></span> Save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>                    
                    </div>
                    
                </div>
            </div>        
            </div>
        </div>
        <!-- End modal Settings-->        
    @endif


    <!-- Scripts -->
    <script src="{{ asset('js/service_control.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Latest compiled and minified JavaScript -->
</body>
</html>
