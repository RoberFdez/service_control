@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit service</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('update-service') }}">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{$service[0]->id}}" name="id">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $service[0]->name }}">  
                                
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">
                                                               
                                <select name="status" id="status" class="form-control">
                                    <option value="1" {{($service[0]->status == 1) ? 'selected' : ''}}>Active</option>
                                    <option value="0" {{($service[0]->status == 0) ? 'selected' : ''}}>Inactive</option>
                                </select> 
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection