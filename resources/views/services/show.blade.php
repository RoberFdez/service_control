@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Services</div>
                <br>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="{{route('create-service')}}" class="btn btn-success">Add new service</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="card-body">
                        <div class="row text-center">
                            <div class="btn-group">
                                <a class="btn btn-primary btn-sm" href="{{route('csv-service')}}">Export CSV</a>
                                <a class="btn btn-primary btn-sm" href="{{route('pdf-service')}}">Export PDF</a>                                
                            </div>
                        </div>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Delete | Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($userServices as $service)
                                    <tr>
                                        <td>{{ $service->name }}</td>
                                        <td>{{ $service->status == 1 ? 'Active' : 'Inactive' }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                                    Options <span class="caret"></span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="btn btn-danger" href="{{route('delete-service', ['id' => $service->id])}}">Delete</a>
                                                    <a class="btn btn-success" href="{{route('edit-service', ['id' => $service->id])}}">Edit</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection