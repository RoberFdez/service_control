<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['userStatus']], function () {
    // Start services routes
    Route::get('show-services', 'ServicesController@index')->name('show-services');
    Route::get('create-service', 'ServicesController@create')->name('create-service');
    Route::post('save-service', 'ServicesController@store')->name('save-service');
    Route::get('edit-service/{id}/id', 'ServicesController@edit')->name('edit-service');
    Route::post('update-service', 'ServicesController@update')->name('update-service');
    Route::get('delete-service/{id}/id', 'ServicesController@destroy')->name('delete-service');
    Route::get('csv-service', 'ServicesController@exportServicesCSV')->name('csv-service');
    Route::get('pdf-service', 'ServicesController@exportServicesPDF')->name('pdf-service');
    // End services routes

    // Start users routes
    Route::get('show-users', 'UsersController@index')->name('show-users');
    Route::get('deactivate-user/{id}/id', 'UsersController@deactivateUser')->name('deactivate-user');
    Route::get('active-user/{id}/id', 'UsersController@activateUser')->name('active-user');
    Route::post('edit-user', 'UsersController@editUser')->name('edit-user');
    // End users routes
});
Auth::routes();

